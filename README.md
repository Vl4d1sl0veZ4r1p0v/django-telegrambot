# СПЕЦКУРС BACKEND-РАЗРАБОТКА НА PYTHON
### Решение тестового задания

Решение представлено в виде двух отдельных сервисов: 
- Телеграм бот
- Django сервер

Телеграм бот работает, как фронтенд, передавая запросы от пользователя на сервер и возвращая ответы от сервера пользователю.

Все взаимодействие происходит через `api`. 

Плюс такого подхода - легко горизонтально масштабировать сервисы. 

---

**Пример сессии взаимодействия:**

![img_1.png](images/img_1.png)

![img.png](images/img.png)
