from django.db import IntegrityError
from django.http import Http404
from django.http import HttpResponse

from src.app.models import User


def me(request):
    try:
        user = User.objects.get(pk=request.GET.get('id'))
    except User.DoesNotExist:
        raise Http404("Question does not exist")
    return HttpResponse(str({
        'phone': user.phone,
        'name': user.name,
        'id': user.id
    }))


def set_user(request):
    id = request.GET.get('id')
    phone = request.GET.get('phone')
    name = request.GET.get('name')
    try:
        User.objects.create(id=int(id), phone=phone, name=name)
    except IntegrityError:
        raise Http404('Already exist')
    return HttpResponse(f'Successfully added User: {id} {phone} {name}')
