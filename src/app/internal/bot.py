import logging
import os

import requests
from requests.exceptions import HTTPError
from telegram import Update
from telegram.ext import CallbackContext
from telegram.ext import CommandHandler
from telegram.ext import Updater

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)
host = os.environ.get('HOST')
updater = Updater(token=os.environ.get('TOKEN'), use_context=True)
dispatcher = updater.dispatcher


def start(update: Update, context: CallbackContext):
    context.bot.send_message(chat_id=update.effective_chat.id, text="I'm a bot, please talk to me!")


def me(update: Update, context: CallbackContext):
    payload = {'id': update.effective_chat.id}
    try:
        response = requests.get(url=f'{host}/api/me', params=payload)
        response.raise_for_status()
        context.bot.send_message(chat_id=update.effective_chat.id, text=response.content.decode('utf-8'))
    except HTTPError:
        context.bot.send_message(chat_id=update.effective_chat.id, text='Your phone is not setted')


def set_phone(update: Update, context: CallbackContext):
    try:
        phone = context.args[0]
        id = int(update.effective_chat.id)
        name = update.effective_chat.username
        logger.info(str(update))
        payload = {
            'id': id,
            'phone': phone,
            'name': name
        }
        response = requests.get(url=f'{host}/api/set_user', params=payload)
        response.raise_for_status()
        context.bot.send_message(chat_id=update.effective_chat.id, text=f'Phone {phone} is setted')
    except HTTPError:
        context.bot.send_message(chat_id=update.effective_chat.id, text='Phone already setted')
    except Exception as ex:
        logger.error(ex)
        context.bot.send_message(chat_id=update.effective_chat.id, text='Incorrect command')


start_handler = CommandHandler('start', start)
me_handler = CommandHandler('me', me)
set_phone_handler = CommandHandler('set_phone', set_phone)
dispatcher.add_handler(start_handler)
dispatcher.add_handler(me_handler)
dispatcher.add_handler(set_phone_handler)
updater.start_polling()
