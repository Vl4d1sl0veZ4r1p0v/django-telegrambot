from django.urls import path

from . import handlers

urlpatterns = [
    path('me', handlers.me, name='me'),
    path('set_user', handlers.set_user, name='set_user'),
]
