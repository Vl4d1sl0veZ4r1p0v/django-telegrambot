from django.db import models


class User(models.Model):
    phone = models.CharField(max_length=11)
    name = models.CharField(max_length=50)
