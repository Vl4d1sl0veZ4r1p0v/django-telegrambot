FROM python:3.8

WORKDIR /app
EXPOSE 8000

COPY requirements.txt /app
COPY src/ /app

RUN pip install -r requirements.txt
RUN pip install .
